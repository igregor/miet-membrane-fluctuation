# MIET-Membrane-Fluctuation


## Name
   MIET-Membrane-Fluctuation

## Description
   This repository contains raw data and code to create the figures and datat presented in the manuscript.


## Authors and acknowledgment
   Tao Chen (tao.chen@phys.uni-goettingen.de)
   Narain Karedla (narain.karedla@kennedy.ox.ac.uk)
   Jörg Enderlein (jenderl@gwdg.de)


## File Inventory

In MembraneFluctuations_AnalysisCode folder 

TaoMembraneFluctuationsFigures.m
    Matlab program that calculates the theoretical curves for Figures 1(c) and 1(d). It needs all the m-files contained in this        repository, and the Matlab data files metals.mat and graphene.mat

LifetimeL.pdf
    Description of the Matlab routine LifetimeL.m used by TaoMembraneFluctuationsFigures.m

LifetimeL.m, DipoleL.m, Fresnel.m, mint2str.m, mim.m
    Matlab routines called by TaoMembraneFluctuationsFigures.m

graphene.mat, metals.mat
    Matlab data files containing the optical dispersion curves for graphene and diverse metals, as needed by TaoMembraneFluctuationsFigures.m

TaoMembraneFluctuationTheory.mat
    Matlab data file containing the computational results of TaoMembraneFluctuationsFigures.m and needed for the Mathematica Notebook TaoMembraneFluctuationsMainFigures.nb

MainAnalysisFluctuation.m
    The primary analysis script  facilitates various routines for analyzing the 'ptu' file: 
    1, extracting single-counts data and TCSPC data from a ptu file; 
    2, computing the intensity correlation function, intensity-time trace, and mean lifetime value for the point measurement;
    3, calculate the intentiy-distance slope;
    4, converting the fluorescence intensity correlation function to height correlation function;
    5, fitting the height correlation function using the primary fitting model 'HeightCorrFit.m'.

*.m 
Other Matlab routines called by MainAnalysisFluctuation.m

In MembraneFluctuations_AnalysisCode_rawDate

Figure*.txt
    Measurement data for all figures in the main text, used by TaoMembraneFluctuationsMainFigures.nb for generating the figures

plot_data.nb
    Mathematica Notebook generating all figures in the main text


In OriginalPtuData folder

deflatedGUVonGold.ptu
    One original data recored by SymphoTime softaware (v. 2.8 PicoQuant GmbH). The data is from a point measurement on the proximal membrane of a deflated GUV.

deflatedGUVonGoldFLIM.ptu
    One original data recored by SymphoTime softaware (v. 2.8 PicoQuant GmbH). The data is from a squre-scan measurement on the proximal membrane of a deflated GUV.
